package gr_perm_pg

import (
	"context"
	"fmt"
	"strings"
)

// Заменитель переменных запроса на реальные, подходящие для PostgreSQL
type variableReplacingQueryBuilder struct {
	baseBuilder queryBuilderInterface
	variableKey string
}

// Конструктор сервиса
func newVariableReplacingQueryBuilder(
	baseBuilder queryBuilderInterface,
	variableKey string,
) queryBuilderInterface {
	return &variableReplacingQueryBuilder{
		baseBuilder: baseBuilder,
		variableKey: variableKey,
	}
}

// Генератор запроса для получения списка разрешений для переданного пользователя
//
// Данный генератор на выходе генерирует релевантный запрос для PostgreSQL, а так же
// набор переменных, необходимых для его выполнения.
func (v variableReplacingQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier, permissionKey, groupKey string,
) (query string, variables []interface{}) {
	query, variables = v.baseBuilder.BuildQuery(ctx, userIdentifier, permissionKey, groupKey)

	variableIdx := 0
	for strings.Index(query, v.variableKey) != -1 {
		variableIdx++

		query = strings.Replace(query, v.variableKey, fmt.Sprintf(`$%v`, variableIdx), 1)
	}

	return
}
