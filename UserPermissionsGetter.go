package gr_perm_pg

import (
	"bitbucket.org/dinamic-rbac/gr-rbac"
	"context"
	"github.com/pkg/errors"
)

// Тип, описывающий фабрику моделей разрешений
type userPermissionFactory = func(group string, permission string) gr_rbac.UserPermissionInterface

// Геттер для получения доступных пользователю разрешений
type userPermissionsGetter struct {
	queryBuilder       queryBuilderInterface
	client             sqlClientInterface
	factory            userPermissionFactory
	userIdPreprocessor UserIdentifierPreprocessor
}

// Конструктор геттера
func newUserPermissionsGetter(
	queryBuilder queryBuilderInterface,
	client sqlClientInterface,
	factory userPermissionFactory,
	userIdPreprocessor UserIdentifierPreprocessor,
) gr_rbac.UserPermissionsGetterInterface {
	return &userPermissionsGetter{
		queryBuilder:       queryBuilder,
		client:             client,
		factory:            factory,
		userIdPreprocessor: userIdPreprocessor,
	}
}

// Получение всех доступных для пользователя разрешений по переданному идентификатору
// В результате должен вернуться массив разрешений или ошибка их получения.
func (u userPermissionsGetter) GetUserPermissions(
	ctx context.Context,
	userIdentifier string,
) (permissions []gr_rbac.UserPermissionInterface, err error) {
	userIdentifier, err = u.userIdPreprocessor(userIdentifier)
	if nil != err {
		return nil, errors.Wrap(err, `failed to preprocess user identifier`)
	}

	query, variables := u.queryBuilder.BuildQuery(ctx, userIdentifier, "perm", "group_id")

	rows, err := u.client.GetRows(ctx, query, variables)
	if nil != err {
		return nil, errors.Wrap(err, `failed to get permissions`)
	}

	permissions = make([]gr_rbac.UserPermissionInterface, 0, len(rows))
	for _, row := range rows {
		permissions = append(permissions, u.factory(row[1], row[0]))
	}

	return
}
