package gr_perm_pg

import (
	"context"
	"errors"
)

// Подставка для тестирования
type sqlClientMock struct {
	Result  [][2]string
	IsError bool
}

// Получение строк с данными разрешений
func (s sqlClientMock) GetRows(ctx context.Context, query string, variables []interface{}) ([][2]string, error) {
	if s.IsError {
		return nil, errors.New(`test err`)
	}

	return s.Result, nil
}
