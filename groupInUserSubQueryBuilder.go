package gr_perm_pg

import "context"

// Генератор подзапросов для получения идентификатора группы из таблицы Пользователи
type groupInUserSubQueryBuilder struct {
	conf    innerConfiguration
	checker requiredFieldCheckerInterface
}

// Конструктор генератора подзапроса
func newGroupInUserSubQueryBuilder(
	conf innerConfiguration,
	checker requiredFieldCheckerInterface,
) groupSubQueryBuilderInterface {
	return &groupInUserSubQueryBuilder{
		conf:    conf,
		checker: checker,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (g groupInUserSubQueryBuilder) ValidateConfig(ctx context.Context) error {
	return g.checker.IsFieldsAvailable(&g.conf, []string{
		`UserTable`,
		`UserGroupField`,
		`UserIdentifierField`,
	})
}

// Генератор подзапроса на получение идентификатора группы
//
// По сути формирует на основе переданного сверху идентификатора подзапрос
// на выборку идентификатора группы
func (g groupInUserSubQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier string,
	roleIdentifier string,
	permissionIdentifier string,
) string {
	return `select usr.` + g.conf.UserGroupField + ` from ` + g.conf.UserTable + ` usr where usr.` + g.conf.UserIdentifierField + ` = ` + userIdentifier
}
