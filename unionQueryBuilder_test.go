package gr_perm_pg

import (
	"context"
	"reflect"
	"testing"
)

func Test_unionQueryBuilder_BuildQuery(t *testing.T) {
	type fields struct {
		unionBuilders []queryBuilderInterface
	}
	type args struct {
		ctx            context.Context
		userIdentifier string
		permissionKey  string
		groupKey       string
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantQuery     string
		wantVariables []interface{}
	}{
		{
			name: "Тестирование генерации объединенного запроса",
			fields: fields{
				unionBuilders: []queryBuilderInterface{
					queryBuilderMock{
						Query:     "select * from users",
						Variables: []interface{}{"1"},
					},
					queryBuilderMock{
						Query:     "select * from roles",
						Variables: []interface{}{"2"},
					},
				},
			},
			args: args{
				ctx:            nil,
				userIdentifier: "1",
				permissionKey:  "perm",
				groupKey:       "gr",
			},
			wantQuery:     "select * from users union select * from roles",
			wantVariables: []interface{}{"1", "2"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := unionQueryBuilder{
				unionBuilders: tt.fields.unionBuilders,
			}
			gotQuery, gotVariables := u.BuildQuery(tt.args.ctx, tt.args.userIdentifier, tt.args.permissionKey, tt.args.groupKey)
			if gotQuery != tt.wantQuery {
				t.Errorf("BuildQuery() gotQuery = %v, want %v", gotQuery, tt.wantQuery)
			}
			if !reflect.DeepEqual(gotVariables, tt.wantVariables) {
				t.Errorf("BuildQuery() gotVariables = %v, want %v", gotVariables, tt.wantVariables)
			}
		})
	}
}
