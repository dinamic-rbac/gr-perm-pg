package gr_perm_pg

import (
	"context"
	"testing"
)

func Test_groupInRoleSubQueryBuilder_BuildQuery(t *testing.T) {
	type fields struct {
		conf    innerConfiguration
		checker requiredFieldCheckerInterface
	}
	type args struct {
		ctx                  context.Context
		userIdentifier       string
		roleIdentifier       string
		permissionIdentifier string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование генерации запросов",
			fields: fields{
				conf: innerConfiguration{
					Configuration: Configuration{
						RoleTable:           "roles",
						RoleGroupField:      "group_id",
						RoleIdentifierField: "id",
					},
				},
				checker: nil,
			},
			args: args{
				ctx:                  nil,
				userIdentifier:       "1",
				roleIdentifier:       "2",
				permissionIdentifier: "3",
			},
			want: "select gr.group_id from roles gr where gr.id = 2",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := groupInRoleSubQueryBuilder{
				conf:    tt.fields.conf,
				checker: tt.fields.checker,
			}
			if got := g.BuildQuery(tt.args.ctx, tt.args.userIdentifier, tt.args.roleIdentifier, tt.args.permissionIdentifier); got != tt.want {
				t.Errorf("BuildQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}
