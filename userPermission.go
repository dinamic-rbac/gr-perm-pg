package gr_perm_pg

import "bitbucket.org/dinamic-rbac/gr-rbac"

// Модель разрешения
type userPermission struct {
	Group      string `json:"group"`
	Permission string `json:"permission"`
}

// Конструктор модели
func newUserPermission(
	group string,
	permission string,
) gr_rbac.UserPermissionInterface {
	return &userPermission{
		Group:      group,
		Permission: permission,
	}
}

// Получение группы, для которой открыто разрешение
func (u userPermission) GetGroup() string {
	return u.Group
}

// Получение идентификатора разрешения
func (u userPermission) GetPermission() string {
	return u.Permission
}
