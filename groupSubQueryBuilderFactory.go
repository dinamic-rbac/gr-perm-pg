package gr_perm_pg

import (
	"context"
	"github.com/pkg/errors"
)

// Фабрика генератора подзапросов получения идентификатора группы
func groupSubQueryBuilderFactory(
	ctx context.Context,
	conf innerConfiguration,
	checker requiredFieldCheckerInterface,
) (result groupSubQueryBuilderInterface, err error) {
	switch conf.GroupIdentifierPosition {
	case GroupInUser:
		result = newGroupInUserSubQueryBuilder(conf, checker)
		break
	case GroupInUserRole:
		result = newGroupInUserRoleSubQueryBuilder(conf, checker)
		break
	case GroupInRole:
		result = newGroupInRoleSubQueryBuilder(conf, checker)
		break
	case GroupInRolePermission:
		result = newGroupInRolePermissionSubQueryBuilder(conf, checker)
		break
	case GroupInPermission:
		result = newGroupInPermissionSubQueryBuilder(conf, checker)
		break
	case GroupInUserPermission:
		result = newGroupInUserPermissionSubQueryBuilder(conf, checker)
		break
	default:
		return nil, errors.New(`you should set correct GroupIdentifierPosition, current variant is unknown`)
	}

	err = result.ValidateConfig(ctx)
	if nil != err {
		return nil, errors.Wrap(err, `failed to check configuration`)
	}

	return
}
