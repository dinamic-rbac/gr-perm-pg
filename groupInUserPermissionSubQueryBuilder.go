package gr_perm_pg

import "context"

// Генератор подзапросов для получения идентификатора группы из таблицы связи Пользователь - Разрешение
type groupInUserPermissionSubQueryBuilder struct {
	conf    innerConfiguration
	checker requiredFieldCheckerInterface
}

// Конструктор генератора подзапросов
func newGroupInUserPermissionSubQueryBuilder(
	conf innerConfiguration,
	checker requiredFieldCheckerInterface,
) groupSubQueryBuilderInterface {
	return &groupInUserPermissionSubQueryBuilder{
		conf:    conf,
		checker: checker,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (g groupInUserPermissionSubQueryBuilder) ValidateConfig(ctx context.Context) error {
	return g.checker.IsFieldsAvailable(&g.conf, []string{
		`UserPermissionRelationTable`,
		`UserPermissionRelationGroupIdentifierField`,
		`UserPermissionRelationUserIdentifierField`,
		`UserPermissionRelationPermissionIdentifierField`,
	})
}

// Генератор подзапроса на получение идентификатора группы
//
// По сути формирует на основе переданного сверху идентификатора подзапрос
// на выборку идентификатора группы
func (g groupInUserPermissionSubQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier string,
	roleIdentifier string,
	permissionIdentifier string,
) string {
	return `select gup.` + g.conf.UserPermissionRelationGroupIdentifierField + ` from ` + g.conf.UserPermissionRelationTable + ` gup where gup.` + g.conf.UserPermissionRelationUserIdentifierField + ` = ` + userIdentifier + ` and gup.` + g.conf.UserPermissionRelationPermissionIdentifierField + ` = ` + permissionIdentifier
}
