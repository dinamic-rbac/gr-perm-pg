package gr_perm_pg

import (
	"context"
	"testing"
)

func Test_groupInUserRoleSubQueryBuilder_BuildQuery(t *testing.T) {
	type fields struct {
		conf    innerConfiguration
		checker requiredFieldCheckerInterface
	}
	type args struct {
		ctx                  context.Context
		userIdentifier       string
		roleIdentifier       string
		permissionIdentifier string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование генерации запросов",
			fields: fields{
				conf: innerConfiguration{
					Configuration: Configuration{
						UserRolesRelationTable:                "users_roles",
						UserRolesRelationGroupIdentifierField: "group_id",
						UserRolesRelationUserIdentifierField:  "user_id",
						UserRolesRelationRoleIdentifierField:  "role_id",
					},
				},
				checker: nil,
			},
			args: args{
				ctx:                  nil,
				userIdentifier:       "1",
				roleIdentifier:       "2",
				permissionIdentifier: "3",
			},
			want: "select gur.group_id from users_roles gur where gur.user_id = 1 and gur.role_id = 2",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := groupInUserRoleSubQueryBuilder{
				conf:    tt.fields.conf,
				checker: tt.fields.checker,
			}
			if got := g.BuildQuery(tt.args.ctx, tt.args.userIdentifier, tt.args.roleIdentifier, tt.args.permissionIdentifier); got != tt.want {
				t.Errorf("BuildQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}
