package gr_perm_pg

import (
	"errors"
	"reflect"
)

// Сервис проверки полей конфигурации
type requiredFieldChecker struct{}

// Конструктор сервиса
func newRequiredFieldChecker() requiredFieldCheckerInterface {
	return &requiredFieldChecker{}
}

// Проверка наличия полей в конфигурации
//
// Проверяет заполненость строковых полей в конфигурации.
// Если какое-то из полей не заполнено, возвращает ошибку.
func (r requiredFieldChecker) IsFieldsAvailable(conf *innerConfiguration, fields []string) error {
	confStruct := reflect.ValueOf(conf).Elem()
	for _, fieldKey := range fields {
		field := confStruct.FieldByName(fieldKey)
		if !field.IsValid() {
			return errors.New(`failed to get field by key "` + fieldKey + `"`)
		}

		value := field.String()
		if 0 == len(value) {
			return errors.New(`field "` + fieldKey + `" has empty value`)
		}
	}

	return nil
}
