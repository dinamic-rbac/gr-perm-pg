package gr_perm_pg

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
)

// Дефолтный плейсхолдер для переменных в запросе
const defaultVariablePlaceholder = "###__VARIABLE_D#$*FKSKLWE_PLACEHOLDER__###"

// Фабрика генератора запросов
func queryBuilderFactory(ctx context.Context, conf ...Configuration) (result queryBuilderInterface, err error) {
	if 0 == len(conf) {
		return nil, errors.New(`you should pass any configuration`)
	}

	checker := newRequiredFieldChecker()
	builders := make([]queryBuilderInterface, 0, len(conf))
	for i, cfg := range conf {
		builder, err := queryBuilderBaseFactory(ctx, cfg, checker, defaultVariablePlaceholder)
		if nil != err {
			return nil, errors.Wrap(err, fmt.Sprintf(`configuration [%v]`, i))
		}

		builders = append(builders, builder)
	}

	if 1 == len(builders) {
		return newVariableReplacingQueryBuilder(builders[0], defaultVariablePlaceholder), nil
	}

	return newVariableReplacingQueryBuilder(
		newUnionQueryBuilder(builders),
		defaultVariablePlaceholder,
	), nil
}

// Фабрика базового генератора запроса по переданной конфигурации
func queryBuilderBaseFactory(
	ctx context.Context,
	conf Configuration,
	checker requiredFieldCheckerInterface,
	variablePlaceholder string,
) (result queryBaseBuilderInterface, err error) {
	innerConf := innerConfiguration{
		Configuration:            conf,
		QueryVariablePlaceholder: variablePlaceholder,
	}

	groupBuilder, err := groupSubQueryBuilderFactory(ctx, innerConf, checker)
	if nil != err {
		return nil, errors.Wrap(err, `failed to create group sub query builder`)
	}

	switch conf.RelationsType {
	case UrpWithUrAndRpRelation:
		result = newUrpWithUrAndRpRelationQueryBuilder(innerConf, groupBuilder, checker)
		break
	case UrpWithRpRelation:
		result = newUrpWithRpRelationQueryBuilder(innerConf, groupBuilder, checker)
		break
	case UrpWithUrRelation:
		result = newUrpWithUrRelationQueryBuilder(innerConf, groupBuilder, checker)
		break
	case UrpWithoutRelation:
		result = newUrpQueryBuilder(innerConf, groupBuilder, checker)
		break
	case UpWithRelation:
		result = newUpWithUpRelationQueryBuilder(innerConf, groupBuilder, checker)
		break
	case UpWithoutRelation:
		result = newUpQueryBuilder(innerConf, groupBuilder, checker)
		break
	default:
		return nil, errors.New(`you should pass correct relation type`)
	}

	err = result.ValidateConfig(ctx)
	if nil != err {
		err = errors.Wrap(err, `failed to check configuration`)

		return nil, err
	}

	if !isGroupAvailable(innerConf.GroupIdentifierPosition, result.AvailableGroupPositions()) {
		return nil, errors.New(`passed group identifier position is not available for chosen relation variant`)
	}

	return
}

// Проверяет доступность типа расположения группы среди переданных вариантов
func isGroupAvailable(group GroupPositionType, variants []GroupPositionType) bool {
	for _, available := range variants {
		if available == group {
			return true
		}
	}

	return false
}
