package gr_perm_pg

import (
	"bitbucket.org/dinamic-rbac/gr-rbac"
	"context"
	"database/sql"
)

// Фабрика геттера разрешений без препроцессинга идентификатора пользователя
func NewUserPermissionsGetter(db *sql.DB, conf ...Configuration) (gr_rbac.UserPermissionsGetterInterface, error) {
	builder, err := queryBuilderFactory(context.Background(), conf...)
	if nil != err {
		return nil, err
	}

	return newUserPermissionsGetter(
		builder,
		newSqlClient(db),
		newUserPermission,
		func(userIdentifier string) (s string, e error) {
			return userIdentifier, nil
		},
	), nil
}

// Фабрика геттера разрешений без препроцессинга идентификатора пользователя
func NewUserPermissionsGetterWithUserIdentifierPreprocessor(
	db *sql.DB,
	userIdPreprocessor UserIdentifierPreprocessor,
	conf ...Configuration,
) (gr_rbac.UserPermissionsGetterInterface, error) {
	builder, err := queryBuilderFactory(context.Background(), conf...)
	if nil != err {
		return nil, err
	}

	return newUserPermissionsGetter(
		builder,
		newSqlClient(db),
		newUserPermission,
		userIdPreprocessor,
	), nil
}
