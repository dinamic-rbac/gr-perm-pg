package gr_perm_pg

import (
	"context"
)

// Сервис генерации запроса для схемы хранения Пользователь - Роль - Разрешение
// с таблицей связи Роль - Разрешение
type urpWithRpRelationQueryBuilder struct {
	conf              innerConfiguration
	groupQueryBuilder groupSubQueryBuilderInterface
	checker           requiredFieldCheckerInterface
}

// Конструктор сервиса
func newUrpWithRpRelationQueryBuilder(
	conf innerConfiguration,
	groupQueryBuilder groupSubQueryBuilderInterface,
	checker requiredFieldCheckerInterface,
) queryBaseBuilderInterface {
	return &urpWithRpRelationQueryBuilder{
		conf:              conf,
		groupQueryBuilder: groupQueryBuilder,
		checker:           checker,
	}
}

// Возвращает список доступных для установки типов генераторов
// подзапроса на получение идентификатора группы
func (u urpWithRpRelationQueryBuilder) AvailableGroupPositions() []GroupPositionType {
	return []GroupPositionType{
		GroupInUser,
		GroupInPermission,
		GroupInRole,
		GroupInRolePermission,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (u urpWithRpRelationQueryBuilder) ValidateConfig(ctx context.Context) error {
	return u.checker.IsFieldsAvailable(&u.conf, []string{
		`PermissionTable`,
		`PermissionLoadingIdentifierField`,
		`PermissionIdentifierField`,
		`RolePermissionsRelationTable`,
		`RolePermissionsPermissionIdentifierField`,
		`RolePermissionsRoleIdentifierField`,
		`UserTable`,
		`UserRolesField`,
		`UserIdentifierField`,
		`QueryVariablePlaceholder`,
	})
}

// Генератор запроса для получения списка разрешений для переданного пользователя
//
// Данный генератор на выходе генерирует релевантный запрос для PostgreSQL, а так же
// набор переменных, необходимых для его выполнения.
func (u urpWithRpRelationQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier, permissionKey, groupKey string,
) (query string, variables []interface{}) {
	groupQuery := u.groupQueryBuilder.BuildQuery(
		ctx,
		`ur.`+u.conf.UserIdentifierField,
		`ur.role_id`,
		`p.`+u.conf.PermissionIdentifierField,
	)

	query = `select p.` + u.conf.PermissionLoadingIdentifierField + `::text as ` + permissionKey + `, (` + groupQuery + `)::text as ` + groupKey + ` from ` + u.conf.PermissionTable + ` p left join ` + u.conf.RolePermissionsRelationTable + ` rp on p.` + u.conf.PermissionIdentifierField + ` = rp.` + u.conf.RolePermissionsPermissionIdentifierField + ` left join (select ` + u.conf.UserIdentifierField + `, unnest(` + u.conf.UserRolesField + `) as role_id from ` + u.conf.UserTable + `) ur on ur.role_id = rp.` + u.conf.RolePermissionsRoleIdentifierField + ` where ur.` + u.conf.UserIdentifierField + ` = ` + u.conf.QueryVariablePlaceholder + `;`

	variables = []interface{}{userIdentifier}

	return
}
