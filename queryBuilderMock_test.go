package gr_perm_pg

import (
	"context"
)

// Подставка для тестирования
type queryBuilderMock struct {
	Query     string
	Variables []interface{}
}

// Генератор запроса для получения списка разрешений для переданного пользователя
//
// Данный генератор на выходе генерирует релевантный запрос для PostgreSQL, а так же
// набор переменных, необходимых для его выполнения.
func (q queryBuilderMock) BuildQuery(
	ctx context.Context,
	userIdentifier, permissionKey, groupKey string,
) (query string, variables []interface{}) {
	return q.Query, q.Variables
}
