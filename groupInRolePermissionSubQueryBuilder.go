package gr_perm_pg

import "context"

// Генератор подзапросов для получения идентификатора группы из таблицы связи Роль - Разрешение
type groupInRolePermissionSubQueryBuilder struct {
	conf    innerConfiguration
	checker requiredFieldCheckerInterface
}

// Конструктор генератора подзапросов
func newGroupInRolePermissionSubQueryBuilder(
	conf innerConfiguration,
	checker requiredFieldCheckerInterface,
) groupSubQueryBuilderInterface {
	return &groupInRolePermissionSubQueryBuilder{
		conf:    conf,
		checker: checker,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (g groupInRolePermissionSubQueryBuilder) ValidateConfig(ctx context.Context) error {
	return g.checker.IsFieldsAvailable(&g.conf, []string{
		`RolePermissionsRelationTable`,
		`RolePermissionsGroupIdentifierField`,
		`RolePermissionsRoleIdentifierField`,
		`RolePermissionsPermissionIdentifierField`,
	})
}

// Генератор подзапроса на получение идентификатора группы
//
// По сути формирует на основе переданного сверху идентификатора подзапрос
// на выборку идентификатора группы
func (g groupInRolePermissionSubQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier string,
	roleIdentifier string,
	permissionIdentifier string,
) string {
	return `select grp.` + g.conf.RolePermissionsGroupIdentifierField + ` from ` + g.conf.RolePermissionsRelationTable + ` grp where grp.` + g.conf.RolePermissionsRoleIdentifierField + ` = ` + roleIdentifier + ` and grp.` + g.conf.RolePermissionsPermissionIdentifierField + ` = ` + permissionIdentifier
}
