package gr_perm_pg

import "context"

// Сервис генерации запроса для схемы хранения Пользователь - Роль - Разрешение
// с таблицей связи Пользователь - Роль
type urpWithUrRelationQueryBuilder struct {
	conf              innerConfiguration
	groupQueryBuilder groupSubQueryBuilderInterface
	checker           requiredFieldCheckerInterface
}

// Конструктор сервиса
func newUrpWithUrRelationQueryBuilder(
	conf innerConfiguration,
	groupQueryBuilder groupSubQueryBuilderInterface,
	checker requiredFieldCheckerInterface,
) queryBaseBuilderInterface {
	return &urpWithUrRelationQueryBuilder{
		conf:              conf,
		groupQueryBuilder: groupQueryBuilder,
		checker:           checker,
	}
}

// Возвращает список доступных для установки типов генераторов
// подзапроса на получение идентификатора группы
func (u urpWithUrRelationQueryBuilder) AvailableGroupPositions() []GroupPositionType {
	return []GroupPositionType{
		GroupInUser,
		GroupInPermission,
		GroupInRole,
		GroupInUserRole,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (u urpWithUrRelationQueryBuilder) ValidateConfig(ctx context.Context) error {
	return u.checker.IsFieldsAvailable(&u.conf, []string{
		`PermissionTable`,
		`PermissionLoadingIdentifierField`,
		`PermissionIdentifierField`,
		`RoleTable`,
		`RoleIdentifierField`,
		`RolePermissionsField`,
		`UserRolesRelationTable`,
		`UserRolesRelationRoleIdentifierField`,
		`UserRolesRelationUserIdentifierField`,
		`QueryVariablePlaceholder`,
	})
}

// Генератор запроса для получения списка разрешений для переданного пользователя
//
// Данный генератор на выходе генерирует релевантный запрос для PostgreSQL, а так же
// набор переменных, необходимых для его выполнения.
func (u urpWithUrRelationQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier, permissionKey, groupKey string,
) (query string, variables []interface{}) {
	groupQuery := u.groupQueryBuilder.BuildQuery(
		ctx,
		`ur.`+u.conf.UserRolesRelationUserIdentifierField,
		`rp.`+u.conf.RoleIdentifierField,
		`p.`+u.conf.PermissionIdentifierField,
	)

	query = `select p.` + u.conf.PermissionLoadingIdentifierField + `::text as ` + permissionKey + `, (` + groupQuery + `)::text as ` + groupKey + ` from ` + u.conf.PermissionTable + ` p left join (select ` + u.conf.RoleIdentifierField + `, unnest(` + u.conf.RolePermissionsField + `) permission_id from ` + u.conf.RoleTable + `) rp on p.` + u.conf.PermissionIdentifierField + ` = rp.permission_id left join ` + u.conf.UserRolesRelationTable + ` ur on ur.` + u.conf.UserRolesRelationRoleIdentifierField + ` = rp.` + u.conf.RoleIdentifierField + ` where ur.` + u.conf.UserRolesRelationUserIdentifierField + ` = ` + u.conf.QueryVariablePlaceholder + `;`
	variables = []interface{}{userIdentifier}

	return
}
