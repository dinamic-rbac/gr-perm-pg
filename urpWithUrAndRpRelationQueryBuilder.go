package gr_perm_pg

import (
	"context"
)

// Генератор запроса получения разрешений для схемы хранения
// Пользователь - Роль - Разрешение с таблицами связи для
// Пользователь - Роль и Роль - Разрешение
type urpWithUrAndRpRelationQueryBuilder struct {
	conf              innerConfiguration
	groupQueryBuilder groupSubQueryBuilderInterface
	checker           requiredFieldCheckerInterface
}

// Конструктор сервиса
func newUrpWithUrAndRpRelationQueryBuilder(
	conf innerConfiguration,
	groupQueryBuilder groupSubQueryBuilderInterface,
	checker requiredFieldCheckerInterface,
) queryBaseBuilderInterface {
	return &urpWithUrAndRpRelationQueryBuilder{
		conf:              conf,
		groupQueryBuilder: groupQueryBuilder,
		checker:           checker,
	}
}

// Возвращает список доступных для установки типов генераторов
// подзапроса на получение идентификатора группы
func (u urpWithUrAndRpRelationQueryBuilder) AvailableGroupPositions() []GroupPositionType {
	return []GroupPositionType{
		GroupInUser,
		GroupInPermission,
		GroupInRole,
		GroupInUserRole,
		GroupInRolePermission,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (u urpWithUrAndRpRelationQueryBuilder) ValidateConfig(ctx context.Context) error {
	return u.checker.IsFieldsAvailable(&u.conf, []string{
		`PermissionTable`,
		`PermissionLoadingIdentifierField`,
		`PermissionIdentifierField`,
		`RolePermissionsRelationTable`,
		`RolePermissionsPermissionIdentifierField`,
		`RolePermissionsRoleIdentifierField`,
		`UserRolesRelationTable`,
		`UserRolesRelationRoleIdentifierField`,
		`UserRolesRelationUserIdentifierField`,
		`QueryVariablePlaceholder`,
	})
}

// Генератор запроса для получения списка разрешений для переданного пользователя
//
// Данный генератор на выходе генерирует релевантный запрос для PostgreSQL, а так же
// набор переменных, необходимых для его выполнения.
func (u urpWithUrAndRpRelationQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier, permissionKey, groupKey string,
) (query string, variables []interface{}) {
	groupQuery := u.groupQueryBuilder.BuildQuery(
		ctx,
		`ur.`+u.conf.UserRolesRelationUserIdentifierField,
		`rp.`+u.conf.RolePermissionsRoleIdentifierField,
		`p.`+u.conf.PermissionIdentifierField,
	)

	query = `select p.` + u.conf.PermissionLoadingIdentifierField + `::text as ` + permissionKey + `, (` + groupQuery + `)::text as ` + groupKey + ` from ` + u.conf.PermissionTable + ` p left join ` + u.conf.RolePermissionsRelationTable + ` rp on p.` + u.conf.PermissionIdentifierField + ` = rp.` + u.conf.RolePermissionsPermissionIdentifierField + ` left join ` + u.conf.UserRolesRelationTable + ` ur on ur.` + u.conf.UserRolesRelationRoleIdentifierField + ` = rp.` + u.conf.RolePermissionsRoleIdentifierField + ` where ur.` + u.conf.UserRolesRelationUserIdentifierField + ` = ` + u.conf.QueryVariablePlaceholder + `;`
	variables = []interface{}{userIdentifier}

	return
}
