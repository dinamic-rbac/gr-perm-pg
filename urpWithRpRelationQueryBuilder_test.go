package gr_perm_pg

import (
	"context"
	"reflect"
	"testing"
)

func Test_urpWithRpRelation_BuildQuery(t *testing.T) {
	type fields struct {
		conf              innerConfiguration
		groupQueryBuilder groupSubQueryBuilderInterface
		checker           requiredFieldCheckerInterface
	}
	type args struct {
		ctx            context.Context
		userIdentifier string
		permissionKey  string
		groupKey       string
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantQuery     string
		wantVariables []interface{}
	}{
		{
			name: "Тестирование генерации запроса",
			fields: fields{
				conf: innerConfiguration{
					Configuration: Configuration{
						PermissionTable:                          "permissions",
						PermissionLoadingIdentifierField:         "id",
						PermissionIdentifierField:                "id",
						RolePermissionsRelationTable:             "roles_permissions",
						RolePermissionsPermissionIdentifierField: "permission_id",
						RolePermissionsRoleIdentifierField:       "role_id",
						UserTable:                                "users",
						UserRolesField:                           "roles",
						UserIdentifierField:                      "id",
					},
					QueryVariablePlaceholder: "$1",
				},
				groupQueryBuilder: &groupSubQueryBuilderMock{Query: "test"},
				checker:           nil,
			},
			args: args{
				ctx:            nil,
				userIdentifier: "1",
				permissionKey:  "permission",
				groupKey:       "group",
			},
			wantQuery:     "select p.id::text as permission, (test)::text as group from permissions p left join roles_permissions rp on p.id = rp.permission_id left join (select id, unnest(roles) as role_id from users) ur on ur.role_id = rp.role_id where ur.id = $1;",
			wantVariables: []interface{}{"1"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := urpWithRpRelationQueryBuilder{
				conf:              tt.fields.conf,
				groupQueryBuilder: tt.fields.groupQueryBuilder,
				checker:           tt.fields.checker,
			}
			gotQuery, gotVariables := u.BuildQuery(tt.args.ctx, tt.args.userIdentifier, tt.args.permissionKey, tt.args.groupKey)
			if gotQuery != tt.wantQuery {
				t.Errorf("BuildQuery() gotQuery = %v, want %v", gotQuery, tt.wantQuery)
			}
			if !reflect.DeepEqual(gotVariables, tt.wantVariables) {
				t.Errorf("BuildQuery() gotVariables = %v, want %v", gotVariables, tt.wantVariables)
			}
		})
	}
}
