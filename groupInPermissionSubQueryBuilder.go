package gr_perm_pg

import (
	"context"
)

// Генератор подзапросов для получения идентификатора группы из таблицы Разрешений
type groupInPermissionSubQueryBuilder struct {
	conf    innerConfiguration
	checker requiredFieldCheckerInterface
}

// Конструктор генератора подзапросов
func newGroupInPermissionSubQueryBuilder(
	conf innerConfiguration,
	checker requiredFieldCheckerInterface,
) groupSubQueryBuilderInterface {
	return &groupInPermissionSubQueryBuilder{
		conf:    conf,
		checker: checker,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (g groupInPermissionSubQueryBuilder) ValidateConfig(ctx context.Context) error {
	return g.checker.IsFieldsAvailable(&g.conf, []string{
		`PermissionTable`,
		`PermissionGroupField`,
		`PermissionIdentifierField`,
	})
}

// Генератор подзапроса на получение идентификатора группы
//
// По сути формирует на основе переданного сверху идентификатора подзапрос
// на выборку идентификатора группы
func (g groupInPermissionSubQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier string,
	roleIdentifier string,
	permissionIdentifier string,
) string {
	return `select gp.` + g.conf.PermissionGroupField + ` from ` + g.conf.PermissionTable + ` gp where gp.` + g.conf.PermissionIdentifierField + ` = ` + permissionIdentifier
}
