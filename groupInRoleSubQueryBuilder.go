package gr_perm_pg

import "context"

// Генератор подзапросов для получения идентификатора группы из таблицы Ролей
type groupInRoleSubQueryBuilder struct {
	conf    innerConfiguration
	checker requiredFieldCheckerInterface
}

// Конструктор генератора подзапросов
func newGroupInRoleSubQueryBuilder(
	conf innerConfiguration,
	checker requiredFieldCheckerInterface,
) groupSubQueryBuilderInterface {
	return &groupInRoleSubQueryBuilder{
		conf:    conf,
		checker: checker,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (g groupInRoleSubQueryBuilder) ValidateConfig(ctx context.Context) error {
	return g.checker.IsFieldsAvailable(&g.conf, []string{
		`RoleTable`,
		`RoleGroupField`,
		`RoleIdentifierField`,
	})
}

// Генератор подзапроса на получение идентификатора группы
//
// По сути формирует на основе переданного сверху идентификатора подзапрос
// на выборку идентификатора группы
func (g groupInRoleSubQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier string,
	roleIdentifier string,
	permissionIdentifier string,
) string {
	return `select gr.` + g.conf.RoleGroupField + ` from ` + g.conf.RoleTable + ` gr where gr.` + g.conf.RoleIdentifierField + ` = ` + roleIdentifier
}
