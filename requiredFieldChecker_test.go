package gr_perm_pg

import "testing"

func Test_requiredFieldChecker_IsFieldsAvailable(t *testing.T) {
	type args struct {
		conf   *innerConfiguration
		fields []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Передано не известное поле",
			args: args{
				conf:   &innerConfiguration{},
				fields: []string{"Test"},
			},
			wantErr: true,
		},
		{
			name: "Передано поле без значения",
			args: args{
				conf:   &innerConfiguration{Configuration: Configuration{UserTable: ""}},
				fields: []string{"UserTable"},
			},
			wantErr: true,
		},
		{
			name: "Передан пустой список полей",
			args: args{
				conf:   &innerConfiguration{Configuration: Configuration{UserTable: ""}},
				fields: []string{},
			},
			wantErr: false,
		},
		{
			name: "Передано 2 поля, поле без значения и со значением",
			args: args{
				conf:   &innerConfiguration{Configuration: Configuration{UserTable: "", UserRolesField: "Test"}},
				fields: []string{"UserTable", "UserRolesField"},
			},
			wantErr: true,
		},
		{
			name: "Передано 2 поля со значением",
			args: args{
				conf:   &innerConfiguration{Configuration: Configuration{UserTable: "Test", UserRolesField: "Test"}},
				fields: []string{"UserTable", "UserRolesField"},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := requiredFieldChecker{}
			if err := r.IsFieldsAvailable(tt.args.conf, tt.args.fields); (err != nil) != tt.wantErr {
				t.Errorf("IsFieldsAvailable() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
