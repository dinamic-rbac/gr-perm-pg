package gr_perm_pg

import (
	gr_rbac "bitbucket.org/dinamic-rbac/gr-rbac"
	"context"
	"errors"
	"reflect"
	"testing"
)

func Test_userPermissionsGetter_GetUserPermissions(t *testing.T) {
	type fields struct {
		queryBuilder       queryBuilderInterface
		client             sqlClientInterface
		factory            userPermissionFactory
		userIdPreprocessor UserIdentifierPreprocessor
	}
	type args struct {
		ctx            context.Context
		userIdentifier string
	}
	tests := []struct {
		name            string
		fields          fields
		args            args
		wantPermissions []gr_rbac.UserPermissionInterface
		wantErr         bool
	}{
		{
			name: "Запрос сформирован, при выполнении запроса произошла ошибка - ошибка в результате",
			fields: fields{
				queryBuilder: queryBuilderMock{
					Query:     "test",
					Variables: nil,
				},
				client: sqlClientMock{
					Result:  nil,
					IsError: true,
				},
				factory: newUserPermission,
				userIdPreprocessor: func(userIdentifier string) (s string, e error) {
					return userIdentifier, nil
				},
			},
			args:            args{},
			wantPermissions: nil,
			wantErr:         true,
		},
		{
			name: "При препроцессинге идентификатора пользователя произошла ошибка - ошибка в результате",
			fields: fields{
				queryBuilder: queryBuilderMock{
					Query:     "test",
					Variables: nil,
				},
				client: sqlClientMock{
					Result:  nil,
					IsError: false,
				},
				factory: newUserPermission,
				userIdPreprocessor: func(userIdentifier string) (s string, e error) {
					return "", errors.New(`test`)
				},
			},
			args:            args{},
			wantPermissions: nil,
			wantErr:         true,
		},
		{
			name: "Запрос сформирован, ошибки выполнения нет - ошибки быть не должно",
			fields: fields{
				queryBuilder: queryBuilderMock{
					Query:     "test",
					Variables: nil,
				},
				client: sqlClientMock{
					Result: [][2]string{
						{"perm1", "group2"},
						{"perm1", "group3"},
						{"perm4", "group1"},
					},
					IsError: false,
				},
				factory: newUserPermission,
				userIdPreprocessor: func(userIdentifier string) (s string, e error) {
					return userIdentifier, nil
				},
			},
			args: args{},
			wantPermissions: []gr_rbac.UserPermissionInterface{
				&userPermission{
					Group:      "group2",
					Permission: "perm1",
				},
				&userPermission{
					Group:      "group3",
					Permission: "perm1",
				},
				&userPermission{
					Group:      "group1",
					Permission: "perm4",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := userPermissionsGetter{
				queryBuilder:       tt.fields.queryBuilder,
				client:             tt.fields.client,
				factory:            tt.fields.factory,
				userIdPreprocessor: tt.fields.userIdPreprocessor,
			}
			gotPermissions, err := u.GetUserPermissions(tt.args.ctx, tt.args.userIdentifier)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetUserPermissions() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotPermissions, tt.wantPermissions) {
				t.Errorf("GetUserPermissions() gotPermissions = %v, want %v", gotPermissions, tt.wantPermissions)
			}
		})
	}
}
