package gr_perm_pg

import "context"

// Генератор подзапросов для получения идентификатора группы из таблицы связи Пользователь - Роль
type groupInUserRoleSubQueryBuilder struct {
	conf    innerConfiguration
	checker requiredFieldCheckerInterface
}

// Конструктор генератора подзапроса
func newGroupInUserRoleSubQueryBuilder(
	conf innerConfiguration,
	checker requiredFieldCheckerInterface,
) groupSubQueryBuilderInterface {
	return &groupInUserRoleSubQueryBuilder{
		conf:    conf,
		checker: checker,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (g groupInUserRoleSubQueryBuilder) ValidateConfig(ctx context.Context) error {
	return g.checker.IsFieldsAvailable(&g.conf, []string{
		`UserRolesRelationTable`,
		`UserRolesRelationGroupIdentifierField`,
		`UserRolesRelationUserIdentifierField`,
		`UserRolesRelationRoleIdentifierField`,
	})
}

// Генератор подзапроса на получение идентификатора группы
//
// По сути формирует на основе переданного сверху идентификатора подзапрос
// на выборку идентификатора группы
func (g groupInUserRoleSubQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier string,
	roleIdentifier string,
	permissionIdentifier string,
) string {
	return `select gur.` + g.conf.UserRolesRelationGroupIdentifierField + ` from ` + g.conf.UserRolesRelationTable + ` gur where gur.` + g.conf.UserRolesRelationUserIdentifierField + ` = ` + userIdentifier + ` and gur.` + g.conf.UserRolesRelationRoleIdentifierField + ` = ` + roleIdentifier
}
