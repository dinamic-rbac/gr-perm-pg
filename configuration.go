package gr_perm_pg

// Тип, описывающий формат отношений для Пользователь - Роль - Разрешение
type RelationVariantsType uint8

// Доступные для указания типы отношений
const (
	// Модель Пользователь - Роль - Разрешение с таблицами связи Пользователь - Роль и Роль - Разрешение
	UrpWithUrAndRpRelation RelationVariantsType = 1

	// Модель Пользователь - Роль - Разрешение с таблицей связи Роль - Разрешение
	UrpWithRpRelation RelationVariantsType = 2

	// Модель Пользователь - Роль - Разрешение с таблицей связи Пользователь - Роль
	UrpWithUrRelation RelationVariantsType = 3

	// Модель Пользователь - Роль - Разрешение без таблиц связи
	UrpWithoutRelation RelationVariantsType = 4

	// Модель Пользователь - Разрешение с таблицей связи
	UpWithRelation RelationVariantsType = 5

	// Модель Пользователь - Разрешение без таблицы связи
	UpWithoutRelation RelationVariantsType = 6
)

// Тип, определяющий положение идентификатора группы в модели Пользователь - Роль - Разрешение
type GroupPositionType uint8

// Доступные для выбора типы расположения идентификатора группы
const (
	// Идентификатор группы располагается в модели Пользователь
	GroupInUser GroupPositionType = 1

	// Идентификатор группы располагается в таблице связи Пользователь - Роль
	GroupInUserRole GroupPositionType = 2

	// Идентификатор группы располагается в модели Роль
	GroupInRole GroupPositionType = 3

	// Идентификатор группы располагается в таблице связи Роль - Разрешение
	GroupInRolePermission GroupPositionType = 4

	// Идентификатор группы располагается в модели Разрешение
	GroupInPermission GroupPositionType = 5

	// Идентификатор группы располагается в таблице связи Пользователь - Разрешение
	GroupInUserPermission GroupPositionType = 6
)

// Конфигурация таблиц для геттера
type Configuration struct {
	UserTable           string // Название таблицы пользователей в БД
	UserRolesField      string // Поле, содержащее массив ролей пользователя в таблице
	UserIdentifierField string // Поле, содержащее идентификатор пользователя в таблице
	UserGroupField      string // Поле, содержащее идентификатор группы в таблице
	UserPermissionField string // Поле, содержащее массив разрешений пользователя в таблице

	RoleTable            string // Название таблицы ролей
	RoleIdentifierField  string // Поле, содержащее идентификатор роли в таблице
	RolePermissionsField string // Поле, содержащее массив разрешений для роли в таблице
	RoleGroupField       string // Поле, содержащее группу в таблице ролей

	PermissionTable                  string // Таблица разрешений
	PermissionLoadingIdentifierField string // Поле, содержащее реальный идентификатор разрешения
	PermissionIdentifierField        string // Поле, содержащее внутренний идентификатор разрешения
	PermissionGroupField             string // Поле, содержащее группу в таблице разрешения

	UserRolesRelationTable                string // Название таблицы отношения Пользователь - Роль
	UserRolesRelationUserIdentifierField  string // Поле с идентификатором пользователя в этой таблице
	UserRolesRelationRoleIdentifierField  string // Поле с идентификатором роли в этой таблице
	UserRolesRelationGroupIdentifierField string // Поле с идентификатором группы в этой таблице

	RolePermissionsRelationTable             string // Название таблицы отношения Роль - Разрешение
	RolePermissionsRoleIdentifierField       string // Поле с идентификатором роли в этой таблице
	RolePermissionsPermissionIdentifierField string // Поле с идентификатором разраешения в этой таблице
	RolePermissionsGroupIdentifierField      string // Поле с идентификатором группы в этой таблице

	UserPermissionRelationTable                     string // Название таблицы отношения Пользователь - Разрешение
	UserPermissionRelationUserIdentifierField       string // Поле с идентификатором пользователя в этой таблице
	UserPermissionRelationPermissionIdentifierField string // Поле с идентификатором роли в этой таблице
	UserPermissionRelationGroupIdentifierField      string // Поле с идентификатором группы в этой таблице

	RelationsType           RelationVariantsType // Тип структуры отношений для модели Пользователь - Роль - Разрешение
	GroupIdentifierPosition GroupPositionType    // Расположение идентификатора группы
}

// Конфигурация таблиц для геттера для внутреннего использования
type innerConfiguration struct {
	Configuration

	// Уникальный плейсхолдер для использования в качестве переменной
	//
	// Используется в генераторе запроса для объединения запросов с переменными
	// В качестве значения по умолчанию используется - ###__VARIABLE_D#$*FKSKLWE_PLACEHOLDER__###
	//
	// Важно! Используйте только уникальные идентификаторы, не совпадающие с названием
	// какого либо поля/таблицы из конфигурации. Так же не рекомендуется использовать
	// зарезервированные выражения.
	QueryVariablePlaceholder string
}
