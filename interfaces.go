package gr_perm_pg

import "context"

// Тип, описывающий препроцессор идентификатора пользователя
type UserIdentifierPreprocessor = func(userIdentifier string) (string, error)

// Интерфейс клиента для выполнения запроса
type sqlClientInterface interface {
	// Получение строк с данными разрешений
	GetRows(ctx context.Context, query string, variables []interface{}) ([][2]string, error)
}

// Сервис проверки полей конфигурации
type requiredFieldCheckerInterface interface {
	// Проверка наличия полей в конфигурации
	//
	// Проверяет заполненость строковых полей в конфигурации.
	// Если какое-то из полей не заполнено, возвращает ошибку.
	IsFieldsAvailable(conf *innerConfiguration, fields []string) error
}

// Интерфейс базового генератора SQL запроса на получение списка разрешений
type queryBaseBuilderInterface interface {
	queryBuilderInterface

	// Возвращает список доступных для установки типов генераторов
	// подзапроса на получение идентификатора группы
	AvailableGroupPositions() []GroupPositionType

	// Валидатор конфигурации построителя запроса
	// Возвращает ошибку, если конфигурация не валидна
	ValidateConfig(ctx context.Context) error
}

// Интерфейс генератора SQL запроса на получение списка разрешений
type queryBuilderInterface interface {
	// Генератор запроса для получения списка разрешений для переданного пользователя
	//
	// Данный генератор на выходе генерирует релевантный запрос для PostgreSQL, а так же
	// набор переменных, необходимых для его выполнения.
	BuildQuery(
		ctx context.Context,
		userIdentifier, permissionKey, groupKey string,
	) (query string, variables []interface{})
}

// Генератор подзапросов для получения идентификатора группы
type groupSubQueryBuilderInterface interface {
	// Валидатор конфигурации построителя запроса
	// Возвращает ошибку, если конфигурация не валидна
	ValidateConfig(ctx context.Context) error

	// Генератор подзапроса на получение идентификатора группы
	//
	// По сути формирует на основе переданного сверху идентификатора подзапрос
	// на выборку идентификатора группы
	BuildQuery(
		ctx context.Context,
		userIdentifier string,
		roleIdentifier string,
		permissionIdentifier string,
	) string
}
