package gr_perm_pg

import "context"

// Сервис генерации запроса для схемы хранения Пользователь - Роль - Разрешение
// без каких либо дополнительных таблиц связи
type urpQueryBuilder struct {
	conf              innerConfiguration
	groupQueryBuilder groupSubQueryBuilderInterface
	checker           requiredFieldCheckerInterface
}

// Конструктор сервиса
func newUrpQueryBuilder(
	conf innerConfiguration,
	groupQueryBuilder groupSubQueryBuilderInterface,
	checker requiredFieldCheckerInterface,
) queryBaseBuilderInterface {
	return &urpQueryBuilder{
		conf:              conf,
		groupQueryBuilder: groupQueryBuilder,
		checker:           checker,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (u urpQueryBuilder) ValidateConfig(ctx context.Context) error {
	return u.checker.IsFieldsAvailable(&u.conf, []string{
		`PermissionTable`,
		`PermissionLoadingIdentifierField`,
		`PermissionIdentifierField`,
		`RoleTable`,
		`RoleIdentifierField`,
		`RolePermissionsField`,
		`UserTable`,
		`UserIdentifierField`,
		`UserRolesField`,
		`QueryVariablePlaceholder`,
	})
}

// Возвращает список доступных для установки типов генераторов
// подзапроса на получение идентификатора группы
func (u urpQueryBuilder) AvailableGroupPositions() []GroupPositionType {
	return []GroupPositionType{
		GroupInUser,
		GroupInPermission,
		GroupInRole,
	}
}

// Генератор запроса для получения списка разрешений для переданного пользователя
//
// Данный генератор на выходе генерирует релевантный запрос для PostgreSQL, а так же
// набор переменных, необходимых для его выполнения.
func (u urpQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier, permissionKey, groupKey string,
) (query string, variables []interface{}) {
	groupQuery := u.groupQueryBuilder.BuildQuery(
		ctx,
		`ur.`+u.conf.UserIdentifierField,
		`rp.`+u.conf.RoleIdentifierField,
		`p.`+u.conf.PermissionIdentifierField,
	)

	query = `select p.` + u.conf.PermissionLoadingIdentifierField + `::text as ` + permissionKey + `, (` + groupQuery + `)::text as ` + groupKey + ` from ` + u.conf.PermissionTable + ` p left join (select ` + u.conf.RoleIdentifierField + `, unnest(` + u.conf.RolePermissionsField + `) permission_id from ` + u.conf.RoleTable + `) rp on p.` + u.conf.PermissionIdentifierField + ` = rp.permission_id left join (select ` + u.conf.UserIdentifierField + `, unnest(` + u.conf.UserRolesField + `) role_id from ` + u.conf.UserTable + `) ur on ur.role_id = rp.` + u.conf.RoleIdentifierField + ` where ur.` + u.conf.UserIdentifierField + ` = ` + u.conf.QueryVariablePlaceholder + `;`
	variables = []interface{}{userIdentifier}

	return
}
