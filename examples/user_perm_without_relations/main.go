package main

import (
	"bitbucket.org/dinamic-rbac/gr-perm-pg"
	"bitbucket.org/dinamic-rbac/gr-perm-pg/examples/pgsql"
	"context"
	"encoding/json"
	"log"
)

// Данный пример описывает структуру хранения RBAC моделей следующей конфигурации:
// Пользователь - Разрешение. В данной конфигурации отсутствует таблица связи,
// связь Пользователь - Разрешение хранится в поле-массиве в соответствующей таблице.
// Для примера расположение группы берется из разрешения.
func main() {
	getter, err := gr_perm_pg.NewUserPermissionsGetter(pgsql.Connection.DB, gr_perm_pg.Configuration{
		PermissionTable:                  "permissions",
		PermissionLoadingIdentifierField: "id",
		PermissionIdentifierField:        "id",
		PermissionGroupField:             "group_id",
		UserTable:                        "users",
		UserIdentifierField:              "id",
		UserPermissionField:              "permissions",
		RelationsType:                    gr_perm_pg.UpWithoutRelation,
		GroupIdentifierPosition:          gr_perm_pg.GroupInPermission,
	})

	if nil != err {
		log.Fatal(err)
	}

	permissions, err := getter.GetUserPermissions(context.Background(), "3")
	if nil != err {
		log.Fatal(err)
	}

	data, err := json.Marshal(permissions)
	if nil != err {
		log.Fatal(err)
	}

	log.Println(string(data))
}
