package main

import (
	"bitbucket.org/dinamic-rbac/gr-perm-pg"
	"bitbucket.org/dinamic-rbac/gr-perm-pg/examples/pgsql"
	"context"
	"encoding/json"
	"log"
)

// Данный пример описывает структуру хранения RBAC моделей следующей конфигурации:
// Пользователь - Роль - Разрешение. В примере используется вариант подключения группы
// из таблицы связи Роль - Разрешение.
func main() {
	getter, err := gr_perm_pg.NewUserPermissionsGetter(pgsql.Connection.DB, gr_perm_pg.Configuration{
		PermissionTable:                  "permissions",
		PermissionLoadingIdentifierField: "id",
		PermissionIdentifierField:        "id",

		// Эти поля описывают таблицу связи и подключение группы из таблицы связи
		RolePermissionsRelationTable:             "roles_permissions",
		RolePermissionsPermissionIdentifierField: "permission_id",
		RolePermissionsRoleIdentifierField:       "role_id",
		RolePermissionsGroupIdentifierField:      "group_id",

		UserRolesRelationTable:               "users_roles",
		UserRolesRelationRoleIdentifierField: "role_id",
		UserRolesRelationUserIdentifierField: "user_id",

		RelationsType:           gr_perm_pg.UrpWithUrAndRpRelation,
		GroupIdentifierPosition: gr_perm_pg.GroupInRolePermission,
	})

	if nil != err {
		log.Fatal(err)
	}

	permissions, err := getter.GetUserPermissions(context.Background(), "1")
	if nil != err {
		log.Fatal(err)
	}

	data, err := json.Marshal(permissions)
	if nil != err {
		log.Fatal(err)
	}

	log.Println(string(data))
}
