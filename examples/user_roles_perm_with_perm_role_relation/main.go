package main

import (
	"bitbucket.org/dinamic-rbac/gr-perm-pg"
	"bitbucket.org/dinamic-rbac/gr-perm-pg/examples/pgsql"
	"context"
	"encoding/json"
	"log"
)

// Данный пример описывает структуру хранения RBAC моделей следующей конфигурации:
// Пользователь - Роль - Разрешение. В данной конфигурации есть таблица связи для
// Роль - Разрешение, связи Пользователь - Роль храняться в поле-массиве в соответствующей
// таблице. Для примера расположение группы берется из таблицы связи.
func main() {
	getter, err := gr_perm_pg.NewUserPermissionsGetter(pgsql.Connection.DB, gr_perm_pg.Configuration{
		PermissionTable:                          "permissions",
		PermissionLoadingIdentifierField:         "id",
		PermissionIdentifierField:                "id",
		RolePermissionsRelationTable:             "roles_permissions",
		RolePermissionsPermissionIdentifierField: "permission_id",
		RolePermissionsRoleIdentifierField:       "role_id",
		RolePermissionsGroupIdentifierField:      "group_id",
		UserTable:                                "users",
		UserRolesField:                           "roles",
		UserIdentifierField:                      "id",
		RelationsType:                            gr_perm_pg.UrpWithRpRelation,
		GroupIdentifierPosition:                  gr_perm_pg.GroupInRolePermission,
	})

	if nil != err {
		log.Fatal(err)
	}

	permissions, err := getter.GetUserPermissions(context.Background(), "2")
	if nil != err {
		log.Fatal(err)
	}

	data, err := json.Marshal(permissions)
	if nil != err {
		log.Fatal(err)
	}

	log.Println(string(data))
}
