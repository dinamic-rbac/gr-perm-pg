package main

import (
	"bitbucket.org/dinamic-rbac/gr-perm-pg"
	"bitbucket.org/dinamic-rbac/gr-perm-pg/examples/pgsql"
	"context"
	"encoding/json"
	"log"
)

// Данный пример описывает структуру хранения RBAC моделей следующей конфигурации:
// Пользователь - Роль - Разрешение. В данной конфигурации отсутствуют таблицы связи,
// связи Пользователь - Роль, Роль - Разрешение храняться в полях массивах в соответствующих
// таблицах. Для примера расположение группы берется из роли.
func main() {
	getter, err := gr_perm_pg.NewUserPermissionsGetter(pgsql.Connection.DB, gr_perm_pg.Configuration{
		PermissionTable:                  "permissions",
		PermissionLoadingIdentifierField: "id",
		PermissionIdentifierField:        "id",
		RoleTable:                        "roles",
		RoleIdentifierField:              "id",
		RolePermissionsField:             "permissions",
		RoleGroupField:                   "group_id",
		UserTable:                        "users",
		UserIdentifierField:              "id",
		UserRolesField:                   "roles",
		RelationsType:                    gr_perm_pg.UrpWithoutRelation,
		GroupIdentifierPosition:          gr_perm_pg.GroupInRole,
	})

	if nil != err {
		log.Fatal(err)
	}

	permissions, err := getter.GetUserPermissions(context.Background(), "3")
	if nil != err {
		log.Fatal(err)
	}

	data, err := json.Marshal(permissions)
	if nil != err {
		log.Fatal(err)
	}

	log.Println(string(data))
}
