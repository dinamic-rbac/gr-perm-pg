package main

import (
	"bitbucket.org/dinamic-rbac/gr-perm-pg"
	"bitbucket.org/dinamic-rbac/gr-perm-pg/examples/pgsql"
	"context"
	"encoding/json"
	"log"
)

// Данный пример описывает структуру хранения RBAC моделей следующей конфигурации:
// Пользователь - Роль - Разрешение. В данной конфигурации есть таблицы связи для
// Роль - Разрешение и Пользователь - Роль.
// Для примера расположение группы берется из таблицы связи.
func main() {
	getter, err := gr_perm_pg.NewUserPermissionsGetter(pgsql.Connection.DB, gr_perm_pg.Configuration{
		PermissionTable:                          "permissions",
		PermissionLoadingIdentifierField:         "id",
		PermissionIdentifierField:                "id",
		RolePermissionsRelationTable:             "roles_permissions",
		RolePermissionsPermissionIdentifierField: "permission_id",
		RolePermissionsRoleIdentifierField:       "role_id",
		UserRolesRelationTable:                   "users_roles",
		UserRolesRelationRoleIdentifierField:     "role_id",
		UserRolesRelationUserIdentifierField:     "user_id",
		UserRolesRelationGroupIdentifierField:    "group_id",
		RelationsType:                            gr_perm_pg.UrpWithUrAndRpRelation,
		GroupIdentifierPosition:                  gr_perm_pg.GroupInUserRole,
	})

	if nil != err {
		log.Fatal(err)
	}

	permissions, err := getter.GetUserPermissions(context.Background(), "1")
	if nil != err {
		log.Fatal(err)
	}

	data, err := json.Marshal(permissions)
	if nil != err {
		log.Fatal(err)
	}

	log.Println(string(data))
}
