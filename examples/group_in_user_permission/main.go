package main

import (
	"bitbucket.org/dinamic-rbac/gr-perm-pg"
	"bitbucket.org/dinamic-rbac/gr-perm-pg/examples/pgsql"
	"context"
	"encoding/json"
	"log"
)

// Данный пример описывает структуру хранения RBAC моделей следующей конфигурации:
// Пользователь - Разрешение. В примере используется получение группы из таблицы связи.
func main() {
	getter, err := gr_perm_pg.NewUserPermissionsGetter(pgsql.Connection.DB, gr_perm_pg.Configuration{
		PermissionTable:                  "permissions",
		PermissionLoadingIdentifierField: "id",
		PermissionIdentifierField:        "id",

		// Эти поля описывают подключение таблицы связи и подключение группы
		UserPermissionRelationTable:                     "users_permissions",
		UserPermissionRelationPermissionIdentifierField: "permission_id",
		UserPermissionRelationUserIdentifierField:       "user_id",
		UserPermissionRelationGroupIdentifierField:      "group_id",

		RelationsType:           gr_perm_pg.UpWithRelation,
		GroupIdentifierPosition: gr_perm_pg.GroupInUserPermission,
	})

	if nil != err {
		log.Fatal(err)
	}

	permissions, err := getter.GetUserPermissions(context.Background(), "3")
	if nil != err {
		log.Fatal(err)
	}

	data, err := json.Marshal(permissions)
	if nil != err {
		log.Fatal(err)
	}

	log.Println(string(data))
}
