create table permissions
(
    id       serial not null
        constraint permissions_pk
            primary key,
    group_id integer default 1
);

INSERT INTO public.permissions (id, group_id) VALUES (1, 1);
INSERT INTO public.permissions (id, group_id) VALUES (5, 5);
INSERT INTO public.permissions (id, group_id) VALUES (3, 3);
INSERT INTO public.permissions (id, group_id) VALUES (4, 4);
INSERT INTO public.permissions (id, group_id) VALUES (2, 2);
create table roles
(
    id          serial not null
        constraint roles_pk
            primary key,
    permissions integer[] default '{}'::integer[],
    group_id    integer   default 1
);

alter table roles
    owner to postgres;

INSERT INTO public.roles (id, permissions, group_id) VALUES (1, '{1,2}', 1);
INSERT INTO public.roles (id, permissions, group_id) VALUES (2, '{2,3,4}', 1);
INSERT INTO public.roles (id, permissions, group_id) VALUES (3, '{1,3,4,5}', 1);
INSERT INTO public.roles (id, permissions, group_id) VALUES (4, '{4,5}', 2);
create table roles_permissions
(
    role_id       integer,
    permission_id integer,
    group_id      integer default 1
);

INSERT INTO public.roles_permissions (role_id, permission_id, group_id) VALUES (1, 1, 1);
INSERT INTO public.roles_permissions (role_id, permission_id, group_id) VALUES (4, 4, 1);
INSERT INTO public.roles_permissions (role_id, permission_id, group_id) VALUES (2, 3, 4);
INSERT INTO public.roles_permissions (role_id, permission_id, group_id) VALUES (1, 2, 2);
INSERT INTO public.roles_permissions (role_id, permission_id, group_id) VALUES (3, 1, 6);
INSERT INTO public.roles_permissions (role_id, permission_id, group_id) VALUES (3, 4, 8);
INSERT INTO public.roles_permissions (role_id, permission_id, group_id) VALUES (4, 5, 2);
INSERT INTO public.roles_permissions (role_id, permission_id, group_id) VALUES (3, 5, 9);
INSERT INTO public.roles_permissions (role_id, permission_id, group_id) VALUES (3, 3, 7);
INSERT INTO public.roles_permissions (role_id, permission_id, group_id) VALUES (2, 4, 5);
INSERT INTO public.roles_permissions (role_id, permission_id, group_id) VALUES (2, 2, 3);
create table users
(
    id          serial not null
        constraint users_pk
            primary key,
    roles       integer[] default '{}'::integer[],
    permissions integer[] default '{}'::integer[],
    group_id    integer   default 1
);

INSERT INTO public.users (id, roles, permissions, group_id) VALUES (1, '{2,4}', '{1}', 1);
INSERT INTO public.users (id, roles, permissions, group_id) VALUES (3, '{4}', '{2}', 3);
INSERT INTO public.users (id, roles, permissions, group_id) VALUES (2, '{3}', '{}', 2);
create table users_permissions
(
    user_id       integer,
    permission_id integer,
    group_id      integer default 1
);

INSERT INTO public.users_permissions (user_id, permission_id, group_id) VALUES (1, 1, 1);
INSERT INTO public.users_permissions (user_id, permission_id, group_id) VALUES (3, 2, 2);
create table users_roles
(
    user_id  integer,
    role_id  integer,
    group_id integer default 1
);

INSERT INTO public.users_roles (user_id, role_id, group_id) VALUES (1, 4, 1);
INSERT INTO public.users_roles (user_id, role_id, group_id) VALUES (3, 4, 1);
INSERT INTO public.users_roles (user_id, role_id, group_id) VALUES (1, 2, 2);
INSERT INTO public.users_roles (user_id, role_id, group_id) VALUES (2, 3, 3);