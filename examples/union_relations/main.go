package main

import (
	"bitbucket.org/dinamic-rbac/gr-perm-pg"
	"bitbucket.org/dinamic-rbac/gr-perm-pg/examples/pgsql"
	"context"
	"encoding/json"
	"log"
)

// Данный пример показывает, как можно использовать несколько типов отношений одновременно.
// Суть примера следующая:
// Есть структура Пользователь - Роль - Разрешение. В данной структуре, помимо связи Роль - Разрешение,
// разрешения привязываются к пользователю еще напрямую через связь Пользователь - Разрешение. Для резолвинга
// разрешений нужно учитывать сразу обе связи.
//
// Данный пример как раз описывает эту концепцию. В остальном пример схож с остальными.
func main() {
	getter, err := gr_perm_pg.NewUserPermissionsGetter(
		pgsql.Connection.DB,
		gr_perm_pg.Configuration{
			PermissionTable:                  "permissions",
			PermissionLoadingIdentifierField: "id",
			PermissionIdentifierField:        "id",
			PermissionGroupField:             "group_id",
			UserTable:                        "users",
			UserIdentifierField:              "id",
			UserPermissionField:              "permissions",
			RelationsType:                    gr_perm_pg.UpWithoutRelation,
			GroupIdentifierPosition:          gr_perm_pg.GroupInPermission,
		},
		gr_perm_pg.Configuration{
			PermissionTable:                  "permissions",
			PermissionLoadingIdentifierField: "id",
			PermissionIdentifierField:        "id",
			RoleTable:                        "roles",
			RoleIdentifierField:              "id",
			RolePermissionsField:             "permissions",
			RoleGroupField:                   "group_id",
			UserTable:                        "users",
			UserIdentifierField:              "id",
			UserRolesField:                   "roles",
			RelationsType:                    gr_perm_pg.UrpWithoutRelation,
			GroupIdentifierPosition:          gr_perm_pg.GroupInRole,
		},
	)

	if nil != err {
		log.Fatal(err)
	}

	permissions, err := getter.GetUserPermissions(context.Background(), "1")
	if nil != err {
		log.Fatal(err)
	}

	data, err := json.Marshal(permissions)
	if nil != err {
		log.Fatal(err)
	}

	log.Println(string(data))
}
