package gr_perm_pg

import "context"

// Сервис генерации запроса для схемы хранения Пользователь - Разрешение
// с использованием таблицы связи
type upWithUpRelationQueryBuilder struct {
	conf              innerConfiguration
	groupQueryBuilder groupSubQueryBuilderInterface
	checker           requiredFieldCheckerInterface
}

// Конструктор сервиса
func newUpWithUpRelationQueryBuilder(
	conf innerConfiguration,
	groupQueryBuilder groupSubQueryBuilderInterface,
	checker requiredFieldCheckerInterface,
) queryBaseBuilderInterface {
	return &upWithUpRelationQueryBuilder{
		conf:              conf,
		groupQueryBuilder: groupQueryBuilder,
		checker:           checker,
	}
}

// Возвращает список доступных для установки типов генераторов
// подзапроса на получение идентификатора группы
func (u upWithUpRelationQueryBuilder) AvailableGroupPositions() []GroupPositionType {
	return []GroupPositionType{
		GroupInUser,
		GroupInPermission,
		GroupInUserPermission,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (u upWithUpRelationQueryBuilder) ValidateConfig(ctx context.Context) error {
	return u.checker.IsFieldsAvailable(&u.conf, []string{
		`PermissionTable`,
		`PermissionLoadingIdentifierField`,
		`PermissionIdentifierField`,
		`UserPermissionRelationTable`,
		`UserPermissionRelationPermissionIdentifierField`,
		`UserPermissionRelationUserIdentifierField`,
		`QueryVariablePlaceholder`,
	})
}

// Генератор запроса для получения списка разрешений для переданного пользователя
//
// Данный генератор на выходе генерирует релевантный запрос для PostgreSQL, а так же
// набор переменных, необходимых для его выполнения.
func (u upWithUpRelationQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier, permissionKey, groupKey string,
) (query string, variables []interface{}) {
	groupQuery := u.groupQueryBuilder.BuildQuery(
		ctx,
		`up.`+u.conf.UserPermissionRelationUserIdentifierField,
		``,
		`p.`+u.conf.PermissionIdentifierField,
	)

	query = `select p.` + u.conf.PermissionLoadingIdentifierField + `::text as ` + permissionKey + `, (` + groupQuery + `)::text as ` + groupKey + ` from ` + u.conf.PermissionTable + ` p left join ` + u.conf.UserPermissionRelationTable + ` up on p.` + u.conf.PermissionIdentifierField + ` = up.` + u.conf.UserPermissionRelationPermissionIdentifierField + ` where up.` + u.conf.UserPermissionRelationUserIdentifierField + ` = ` + u.conf.QueryVariablePlaceholder + `;`
	variables = []interface{}{userIdentifier}

	return
}
