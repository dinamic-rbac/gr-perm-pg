package gr_perm_pg

import (
	"context"
	"strings"
)

// Объединитель запросов получения списка разрешений.
//
// При помощи UNION соединяет несколько запросов в один, объединяя наборы
// переменных для этих запросов
type unionQueryBuilder struct {
	unionBuilders []queryBuilderInterface
}

// Конструктор генератора запросов
func newUnionQueryBuilder(unionBuilders []queryBuilderInterface) queryBuilderInterface {
	return &unionQueryBuilder{
		unionBuilders: unionBuilders,
	}
}

// Генератор запроса для получения списка разрешений для переданного пользователя
//
// Данный генератор на выходе генерирует релевантный запрос для PostgreSQL, а так же
// набор переменных, необходимых для его выполнения.
func (u unionQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier, permissionKey, groupKey string,
) (query string, variables []interface{}) {
	variables = make([]interface{}, 0, len(u.unionBuilders))
	queries := make([]string, 0, len(u.unionBuilders))

	for _, builder := range u.unionBuilders {
		query, vars := builder.BuildQuery(ctx, userIdentifier, permissionKey, groupKey)

		queries = append(queries, query)
		variables = append(variables, vars...)
	}

	query = strings.Replace(strings.Join(queries, " union "), ";", "", -1)

	return
}
