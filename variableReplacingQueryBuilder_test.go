package gr_perm_pg

import (
	"context"
	"reflect"
	"testing"
)

func Test_variableReplacingQueryBuilder_BuildQuery(t *testing.T) {
	type fields struct {
		baseBuilder queryBuilderInterface
		variableKey string
	}
	type args struct {
		ctx            context.Context
		userIdentifier string
		permissionKey  string
		groupKey       string
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantQuery     string
		wantVariables []interface{}
	}{
		{
			name: "Тестирование замены переменных",
			fields: fields{
				baseBuilder: queryBuilderMock{
					Query:     "select * from roles where id = ###VAR### and code = ###VAR### and name = ###VAR###",
					Variables: []interface{}{"1", "3", "test"},
				},
				variableKey: "###VAR###",
			},
			args: args{
				ctx:            nil,
				userIdentifier: "1",
				permissionKey:  "2",
				groupKey:       "3",
			},
			wantQuery:     "select * from roles where id = $1 and code = $2 and name = $3",
			wantVariables: []interface{}{"1", "3", "test"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := variableReplacingQueryBuilder{
				baseBuilder: tt.fields.baseBuilder,
				variableKey: tt.fields.variableKey,
			}
			gotQuery, gotVariables := v.BuildQuery(tt.args.ctx, tt.args.userIdentifier, tt.args.permissionKey, tt.args.groupKey)
			if gotQuery != tt.wantQuery {
				t.Errorf("BuildQuery() gotQuery = %v, want %v", gotQuery, tt.wantQuery)
			}
			if !reflect.DeepEqual(gotVariables, tt.wantVariables) {
				t.Errorf("BuildQuery() gotVariables = %v, want %v", gotVariables, tt.wantVariables)
			}
		})
	}
}
