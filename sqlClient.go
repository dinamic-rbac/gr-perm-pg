package gr_perm_pg

import (
	"context"
	"database/sql"
	"github.com/pkg/errors"
)

// Интерфейс клиента для выполнения запроса
type sqlClient struct {
	client *sql.DB
}

// Конструктор клиента
func newSqlClient(client *sql.DB) sqlClientInterface {
	return &sqlClient{
		client: client,
	}
}

// Получение строк с данными разрешений
func (s sqlClient) GetRows(ctx context.Context, query string, variables []interface{}) ([][2]string, error) {
	rows, err := s.client.QueryContext(ctx, query, variables...)
	if nil != err {
		return nil, errors.Wrap(err, `failed to query permissions`)
	}

	stepSize := 100
	result := make([][2]string, 0, stepSize)

	for rows.Next() {
		if cap(result) == len(result) {
			newResult := make([][2]string, 0, cap(result)*2)

			for _, data := range result {
				newResult = append(newResult, data)
			}

			result = newResult
		}

		data := [2]string{}
		err = rows.Scan(&data[0], &data[1])
		if nil != err {
			return nil, errors.Wrap(err, `failed to parse result`)
		}

		result = append(result, data)
	}

	_ = rows.Close()

	return result, nil
}
