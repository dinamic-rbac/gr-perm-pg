package gr_perm_pg

import (
	"context"
)

// Подставка для тестирования
type groupSubQueryBuilderMock struct {
	Query string
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (g groupSubQueryBuilderMock) ValidateConfig(ctx context.Context) error {
	return nil
}

// Генератор подзапроса на получение идентификатора группы
//
// По сути формирует на основе переданного сверху идентификатора подзапрос
// на выборку идентификатора группы
func (g groupSubQueryBuilderMock) BuildQuery(
	ctx context.Context,
	userIdentifier string,
	roleIdentifier string,
	permissionIdentifier string,
) string {
	return g.Query
}
