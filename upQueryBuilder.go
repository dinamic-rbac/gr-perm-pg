package gr_perm_pg

import "context"

// Сервис генерации запроса для схемы хранения Пользователь - Разрешение
// без использования таблицы связи
type upQueryBuilder struct {
	conf              innerConfiguration
	groupQueryBuilder groupSubQueryBuilderInterface
	checker           requiredFieldCheckerInterface
}

// Конструктор сервиса
func newUpQueryBuilder(
	conf innerConfiguration,
	groupQueryBuilder groupSubQueryBuilderInterface,
	checker requiredFieldCheckerInterface,
) queryBaseBuilderInterface {
	return &upQueryBuilder{
		conf:              conf,
		groupQueryBuilder: groupQueryBuilder,
		checker:           checker,
	}
}

// Возвращает список доступных для установки типов генераторов
// подзапроса на получение идентификатора группы
func (u upQueryBuilder) AvailableGroupPositions() []GroupPositionType {
	return []GroupPositionType{
		GroupInUser,
		GroupInPermission,
	}
}

// Валидатор конфигурации построителя запроса
// Возвращает ошибку, если конфигурация не валидна
func (u upQueryBuilder) ValidateConfig(ctx context.Context) error {
	return u.checker.IsFieldsAvailable(&u.conf, []string{
		`PermissionTable`,
		`PermissionLoadingIdentifierField`,
		`PermissionIdentifierField`,
		`UserTable`,
		`UserIdentifierField`,
		`UserPermissionField`,
		`QueryVariablePlaceholder`,
	})
}

// Генератор запроса для получения списка разрешений для переданного пользователя
//
// Данный генератор на выходе генерирует релевантный запрос для PostgreSQL, а так же
// набор переменных, необходимых для его выполнения.
func (u upQueryBuilder) BuildQuery(
	ctx context.Context,
	userIdentifier, permissionKey, groupKey string,
) (query string, variables []interface{}) {
	groupQuery := u.groupQueryBuilder.BuildQuery(
		ctx,
		`up.`+u.conf.UserIdentifierField,
		``,
		`p.`+u.conf.PermissionIdentifierField,
	)

	query = `select p.` + u.conf.PermissionLoadingIdentifierField + `::text as ` + permissionKey + `, (` + groupQuery + `)::text as ` + groupKey + ` from ` + u.conf.PermissionTable + ` p left join (select ` + u.conf.UserIdentifierField + `, unnest(` + u.conf.UserPermissionField + `) permission_id from ` + u.conf.UserTable + `) up on p.` + u.conf.PermissionIdentifierField + ` = up.permission_id where up.` + u.conf.UserIdentifierField + ` = ` + u.conf.QueryVariablePlaceholder + `;`
	variables = []interface{}{userIdentifier}

	return
}
